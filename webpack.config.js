const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: {
	  app: './src/index.js',
		newsPage: './src/newsPage.js'
	},
	mode: "development",
	devServer: {
		port: 9000
	},
	module: {
		rules: [
			{
				test: /\.css$/i,
				use: ["style-loader", "css-loader"],
			},
		],
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].js',
	},
	plugins: [
		new HtmlWebpackPlugin({
			filename: "index.html",
			template: "src/index.html",
			chunks: ['app']

		}),
		new HtmlWebpackPlugin({
			filename: 'newsPage.html',
			template: "src/newsPage.html",
			chunks: ['newsPage']
		})
	]
};