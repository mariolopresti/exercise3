import './style.css';
import axios from "axios";

const inputButton = document.querySelector('#inputButton');
const newsList = document.querySelector('#news-list');

const getNews = async () => {
	//e.preventDefault();
	const endPoint = `http://localhost:5000/news`;
	let res = await axios.get(endPoint);

	newsList.innerHTML = '';

	[...res.data].forEach( element => {
		console.log(element)
		const {id, title, body} = element
		const li = document.createElement('li');
		li.className = 'news';
		li.innerHTML = `<h3>${title}</h3><h5>${body}</h5> <input id="expand-button" type="button" onclick="location.href='http://localhost:9000/newsPage.html?${id}';" value="Expand" />`
		newsList.appendChild(li);
	})

};

getNews();

inputButton.addEventListener('click', getNews);
