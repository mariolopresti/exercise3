import './stylePageNews.css';
import axios from "axios";

const newsList = document.querySelector('#news-list');

let loadNews = async function () {
	console.log(window.location.href)
	const path = window.location.href;
	const newsId = path.toString().split('?')[1];

	const endPoint = `http://localhost:5000/news/${newsId}`;
	let res = await axios.get(endPoint);
	const {id, author, title, body} = {...res.data}
	const li = document.createElement('li');
	li.className = 'news';
	li.innerHTML = `<h3>Title: ${title}</h3><h4>Author: ${author}</h4><h5>${body}</h5>`
	newsList.appendChild(li);

}

loadNews();

